#!/bin/bash
declare TASK_LIST="address netmask gateway"
declare -A ANSWERS

getinfo()
{
MSG_STR_ADDRESS="Inserire l'indirizzo IP per il server FOG: (ad esempio 10.0.123.12)"
MSG_STR_NETMASK="Inserire la netmask per la rete: (ad esempio 255.255.255.0)"
MSG_STR_GATEWAY="Inserire l'indirizzo IP del gateway: (ad esempio 10.0.123.1)"
#
#	Action
#
	# Work the list
	for task in $TASK_LIST
	do
		# Prepare the message string
		str=MSG_STR_${task^^}
		# Loop while input is empty
		while [ -z "${ANSWERS[$task]}" ]
		do
			echo ${!str}
			read input
      if [[ $input =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        ANSWERS[$task]=$input
     else
        #read input
        echo ""
        echo "Verificare formato inserito!!"
        echo ""
      fi
    done
	done
	echo
	echo "Configurazione inserita:"
	for item in $TASK_LIST
	do
    echo "$item: ${ANSWERS[$item]}"
	done
}

writeinterfacefile()
{
ETH=$(ip link | awk -F: '$0 !~ "lo|vir|wl|^[^0-9]"{print $2;getline}')
cp "$1" "$1.bak"
cat << EOF > "$1"
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).
# The loopback network interface
auto lo
iface lo inet loopback

#Your static network configuration
iface $ETH inet static
address ${ANSWERS[address]}
netmask ${ANSWERS[netmask]}
gateway ${ANSWERS[gateway]}
EOF
#don't use any space before of after 'EOF' in the previous line

  echo ""
  echo "La configurazione è stata salvata in '$1'."
  echo "È stata creata una copia di sicurezza '$1.bak'"
  echo ""
  exit 0
}

file="/etc/network/interfaces"
if [ ! -f $file ]; then
  echo ""
  echo "Il file '$file' non esiste!"
  echo ""
  exit 1
fi

clear
echo "Impostare configurazione IP statica per il server FOG"
echo ""

getinfo
echo "Per piacere verificare la configurazione inserita!"

while true; do
  read -p "Queste informazioni sono corrette? [s/n]: " sn
  case $sn in
    [SsYy]* ) writeinterfacefile $file;;
    [Nn]* ) unset ANSWERS; declare -A ANSWERS; getinfo;;
        * ) echo "Per piacere premere s o n!";;
  esac
done