#!/bin/bash
#### Filename: setup.sh
#### Creation Date: 2019-06-03T10:51:47+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-06-03T10:51:51+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later

#### Description: This scripts executes the first setups before running
#### the FOG installation
set -eo pipefail

# Define variables
## Define sleeptime between echos and scripts to help humans read the echo output
sleep_time=3

# Check if running as root
if [[ "$EUID" = 0 ]]; then
    echo "Siamo già root"
else
    sudo -k # make sure to ask for password on next sudo
    if sudo true; then
        echo "Password inserita correttamente"
    else
        echo "Password sudo errata"
        exit 1
    fi
fi

# Define scripts
## Ask for IP/Netmask/Gateway and set parameters
. ./scripts/ethInterface.sh

## Ask for hostname and edit the FOG server hostname
. ./scripts/fogHostname.sh

## Ask for school name, this parameter will be used in FOG for the Location Plugin
. ./scripts/schoolLocation.sh

## SCRIPT0 add gitlab.com to /etc/squid3/squid-added-repo.conf
echo "Aggiungi gitlab.com e snets.it alle ACL di Squid sul server FUSS"
SCRIPT0=./scripts/fuss/addACL.sh

echo "Aggiungi il server FOG agli HOST con permesso di collegarsi ad internet sul server FUSS"
SCRIPT1=./scripts/fuss/fogAllowedHost.sh

# Setup proxy
echo "Imposto i dati del Gateway inseriti come dati di connessione al proxy"
sleep $sleep_time
SCRIPT2=./scripts/proxySetup.sh
sed -i.bak 's/FOGIP/'"${ANSWERS[address]}"'/g' $SCRIPT1 $SCRIPT2
sed -i.bak 's/FOGHOSTNAME/'"${ANSWERS[hostname]}"'/g' $SCRIPT1
. ./scripts/proxySetup.sh

## SCRIPT3 add SSH keys for FUSS Client automatization
echo "Abilito chiavi SSH per client FUSS"
sleep $sleep_time
SCRIPT3=./scripts/editSSH.sh

# Ask if human is present
read -n 1 -s -r -p "Premere un tasto sulla tastiera per continuare, sarà richiesta la password di root del server FUSS"

# Connect to FUSS Server and edit ACLs and Firewall
(
  cat $SCRIPT0 $SCRIPT1 $SCRIPT3
) | ssh -t root@${ANSWERS[gateway]}

# Update installation scripts
echo "Aggiornamento dei file di installazione dal repository su Gitlab"
sleep $sleep_time
git pull

# Restart sshd
echo "Riavviare sshd"
. ./scripts/restartSSHd.sh

# Update ssh keys
echo "Aggiornamento delle chiavi SSH dal server ftp.snets.it"
. ./scripts/addSSH.sh

# Copy SSH keys to FUSS server
echo "Attivare il portachiavi per automatizzare l'installazione di FUSS client"
. ./scripts/activateKeys.sh

# run install
echo "Avvio dell'installazione di FOG"
sleep $sleep_time
. ./install.sh
