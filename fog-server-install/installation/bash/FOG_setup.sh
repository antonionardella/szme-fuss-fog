#!/bin/bash
#### Filename: FOG_setup.sh
#### Creation Date: 2019-06-03T16:17:57+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-06-03T16:18:02+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later

# COPY ME TO THE DESKTOP :)

# If internet is already available update everything
## delete old FOG installation log if present
mkdir -p /var/log/szme
FOGLOG="/var/log/szme/foginstallation.log"
if [ -f $FOGLOG ] ; then
    rm $FOGLOG
fi
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>/var/log/szme/foginstallation.log
# Everything below will  o to the file '/var/log/szme/foginstallation.log'

# Check if running as root
if [[ "$EUID" = 0 ]]; then
    echo "Siamo già root"
else
    sudo -k # make sure to ask for password on next sudo
    if sudo true; then
        echo "Password inserita correttamente"
    else
        echo "Password sudo errata"
        exit 1
    fi
fi

# Check if internet connection is available
# if true update installation with git pull
# else continue with the available version
wget -q --spider http://google.com

if [ $? -eq 0 ]; then
    echo "Online"
    cd /usr/share/szme-fuss-fog
    git pull
else
    echo "Offline"
fi

echo "################################################"
echo "######### FOG Setup script version 0.6.0 #######"
echo "################################################"
sleep 7
# Ask if human is present
echo ""
echo ""
read -n 1 -s -r -p "Prima di procedere con l'installazione accertarsi di aver fatto uno snapshot del server FUSS"
sleep $sleep_time
xfce4-terminal -e "bash -c \"cd /usr/share/szme-fuss-fog/fog-server-install/installation/bash/ && bash setup.sh; bash\""
