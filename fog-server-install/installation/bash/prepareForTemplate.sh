#!//bin/bash
#### Filename: prepareForTemplate.sh
#### Creation Date: 2019-06-06T08:56:41+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-06-06T08:56:44+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later

# clear machine id
echo "clear machine id"
> /etc/machine-id

# clear host SSH keys and /root
echo "clear host SSH keys and /root"
rm -f /etc/ssh/ssh_host_*
rm -rf /root/.ssh/
rm -f /root/anaconda-ks.cfg
rm -f /root/.bash_history
unset HISTFILE

# clear logs
echo "clear logs"
rm -f /var/log/boot.log
rm -f /var/log/cron
rm -f /var/log/dmesg
rm -f /var/log/grubby
rm -f /var/log/lastlog
rm -f /var/log/maillog
rm -f /var/log/messages
rm -f /var/log/secure
rm -f /var/log/spooler
rm -f /var/log/tallylog
rm -f /var/log/wpa_supplicant.log
rm -f /var/log/wtmp
rm -f /var/log/yum.log
rm -f /var/log/audit/audit.log
rm -f /var/log/ovirt-guest-agent/ovirt-guest-agent.log
rm -f /var/log/tuned/tuned.log

# apt clean
echo "apt clean"
apt -y clean
