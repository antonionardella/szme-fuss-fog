#!/bin/bash
#### Filename: update.sh
#### Creation Date: 2019-07-16T14:27:58+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-07-16T14:28:02+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later
# This script loads eventual updates
set -eo pipefail

# Define variables
## Define sleeptime between echos and scripts to help humans read the echo output
sleep_time=3
if [[ "$EUID" = 0 ]]; then
    echo "Siamo già root"
else
    sudo -k # make sure to ask for password on next sudo
    if sudo true; then
        echo "Password inserita correttamente"
    else
        echo "Password sudo errata"
        exit 1
    fi
fi

# Check if internet connection is available
# if true update installation with git pull
# else continue with the available version
wget -q --spider http://google.com

if [ $? -eq 0 ]; then
    echo "Online"
    cd /usr/share/szme-fuss-fog
    git pull
else
    echo "Offline"
fi

# Install packages
echo "Install packages"
DEBIAN_FRONTEND=noninteractive DEBIAN_PRIORITY=critical apt-get -q -y -o "Dpkg::Options::=--force-confdef" -o "Dpkg::Options::=--force-confold" install sudo

# Create link to this file on the desktop
CREATELINK() {
  ln -s /usr/share/szme-fuss-fog/fog-server-install/installation/bash/update.sh /root/Desktop/
}

DELETELINK() {
  rm /root/Desktop/update.sh
}
if [ ! -f /root/Desktop/update.sh ]; then CREATELINK; else DELETELINK && CREATELINK; fi


# Update fog.fusssetup postInstallation script with updates
cd /usr/share/szme-fuss-fog/fog-server-install/installation/bash
. ./scripts/fogPersonalization/updatePostInstall.sh

# get 1.5.4 kernel for older HP clients
cd /usr/share/szme-fuss-fog/fog-server-install/installation/bash
. ./scripts/fogPersonalization/getKernel.sh

# recopy ssh keys for fuss client automation
cd /usr/share/szme-fuss-fog/fog-server-install/installation/bash
. ./scripts/fogPersonalization/ssh-copy-id.sh
