#!/bin/bash
# SOURCE: https://www.unix.com/shell-programming-and-scripting/264382-read-input-keyboard-do-not-proceed-if-no-input.html
#	Variables
#
	# Expand the task list with words that describe the task
	declare TASK_LIST="name email"
	declare -A ANSWERS
#
#	Messages
#
	# Use the words from the task-list as string identifier
	MSG_STR_NAME="What is your name"
	MSG_STR_EMAIL="What is your email"
#
#	Action
#
	# Work the list
	for task in $TASK_LIST
	do
		# Prepare the message string
		str=MSG_STR_${task^^}
		# Loop while input is empty
		while [ -z "${ANSWERS[$task]}" ]
		do
			echo ${!str}
			read input
			ANSWERS[$task]=$input
		done
	done
#
#	Your work to do....
#
	echo
	echo "Result values are:"
	for item in $TASK_LIST
	do
		echo "$item :: ${ANSWERS[$item]}"
	done
