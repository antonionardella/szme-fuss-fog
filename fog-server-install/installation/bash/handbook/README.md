---
author:
- |
    Antonio Nardella\
    antonio.nardella\@provincia.bz.it\
    progetti\@antonionardella.it
title: Installazione FOG in ambiente FUSS da template ProxMox
---

Introduzione
------------

Questo docuento serve da manuale per guidare l'installazione di FOG
<https://fogproject.org>. FOG è installato importando il server virtuale
preconfigurato in ProxMox ed avviando lo script di installazione
`FOG_Setup.sh`. Questo manuale è attualizzato alla versione 0.5 di
`FOG_Setup.sh`.

Prerequisiti
------------

-   Ambiente con server ProxMox

-   Ambiente con FUSS server installato (fuss-server create)

-   Connessione ad internet funzionante

-   Password di root del server FUSS

-   Informazioni su `IP/Netmask/Gateway/Hostname` del server FOG che si
    sta per installare

-   Immagine del server virtuale FOG

-   Dati di accesso al server `ftp.snets.it`

-   OPZIONALE: Parametri vLAN

Lo script di setup prevede una configurazione con server ProxMox. Sul
server ProxMox un server FUSS con le funzionalità standard di DNS,
Firewall (Dansguardian), Proxy (Squid), DHCP ed openLDAP.

![Ambiente di default](environment.png)

Panoramica di installazione
===========================

Import del server virtuale FOG in ProxMox
-----------------------------------------

### Scaricare l'immagine del server virtuale FOG

Scaricare l'immagine del server virtuale FOG per ProxMox dal server FTP
`ftp.snets.it` dalla cartella `KONZEPTE/FUSS/FOG` il nome dell'immagine
è `vzdump-qemu-105-####_##_##-server_fog.vma`.

### Importare l'immagine del server virtuale FOG in ProxMox

Rendere l'immagine del server virtuale FOG scaricato disponibile al
server ProxMox utilizzando il metodo preferito e.g. share NFS, curlftps,
HDD esterno, rsync, scp Importare l'immagine del server virtuale FOG
utilizzando `qmrestore` e.g.
`qmrestore /mnt/discoesterno/vzdump-qemu-105-2019_07_01-server_fog.vma 105 --storage <inserire storage da ProxMox> `

e.g. In ProxMox lo storage di destinazione sarà `pve-ladon` come da
seguente immagine:
![pve-data sarà lo storage di destinazione](qmrestore_storage.png)
Il comando a questo punto sarà
`qmrestore /mnt/discoesterno/vzdump-qemu-105-2019_07_01-server_fog.vma 105 --storage pve-ladon`

guida ufficiale per qmrestore: `man qmrestore` guida ufficiale ProxMox:
<https://pve.proxmox.com/wiki/Backup_and_Restore>

### OPZIONALE: Aggiungere tag VLAN

Di default l'immagine FOG non ha TAG o Queue vLAN impostati. Aggiungere
TAG e/o Queue vLAN in ProxMox se necessario.

### Avviare il server virtuale FOG

Ora avviare il nuovo server virtuale FOG clonato e procedere con la
configurazione. username: `root` password: `fuss`

Configurazione server FOG
-------------------------

Lo script `FOG_Setup.sh` si trova sul desktop. Avviare lo script per
configurare il server.La spiegazione completa si trova nella sezione
successiva:

-   `FOG_setup.sh`Controlla se la connessione internet è disponibile ed
    aggiorna gli script con `git pull` altrimenti continua con gli
    script attuali avviando `setup.sh`

-   `setup.sh`Richiede dati come `IP/Netmask/Gateway/Hostname` e prepara
    il server FOG

-   Connessione al server FUSS per aggiornare le ACL di Squid ed
    aggiungere il server FOG al firewall

-   Installazione di FOG

-   Configurazione del server FUSS

-   Personalizzazioni

Fasi dell'installazione
-----------------------

### FOG\_setup.sh

`FOG_setup.sh`Controlla se la connessione internet è disponibile ed
aggiorna gli script con `git pull` altrimenti continua con gli script
attuali avviando `setup.sh` in un terminale.

### setup.sh

`setup.sh` Questo script richiede all'utente le informazioni necessarie
per procedere con l'installazione semi-automatizzata. Le fasi sono:

-   Richiesta dati della configurazione di rete `IP; Netmask ;Gateway`

-   Richiesta `Hostname` per il server FOG

-   Richiesta `Nome scuola` per il `Location` plugin di FOG

Dopo aver inserito i dati lo script imposta il valore del `Gateway`
inserito dall'utente attraverso lo script `proxySetup.sh` per la
connessione ad internet. L'installazione continua dopo aver premuto un
tasto sulla tastiera, collegandosi via `ssh` al server FUSS ed avviando
gli script `addACL.sh` e `fogAllowedHost`. In questa fase sarà
richiesta la password di `root` del server FUSS in questione.
`addACL.sh` aggiunge le acl `acl repositories url_regex gitlab.com`
`acl repositories url_regex *.snets.it` al file di configurazione
`/etc/squid3/squid-added-repo.conf`.

![ACL squid per accesso a GitLab](squid_acl.png)

Esegue un reload di `squid3` ed esegue un commit di `etckeeper` per
poter tenere traccia della modifica. `fogAllowedHost.sh` aggiunge l'`IP`
ed il `HOSTNAME` configurato in precedenza dall'utente al file
`/etc/fuss-server/firewall-allowed-lan-hosts` sul server FUSS.

![Permettere l'accesso ad internet al server FOG](firewall_fog.png)

Esegue un reload del `firewall` ed esegue un commit di `etckeeper` per
poter tenere traccia della modifica. Al termine si avvia l'installazione
e configurazione di FOG e del server FUSS con lo script `install.sh`.

-   `proxySetup.sh` imposta i parametri del proxy per permettere
    all'immagine di collegarsi alla rete utilizzando il server FUSS come
    proxy `editSSH.sh` abilita l'utilizzo delle chiavi pubbliche per
    collegarsi al server FUSS e riavvia il servizio SSH (Attenzione,
    questo script può interrompere tutte le attuali connessioni SSH al
    server FUSS).

-   `addSSH.sh` in questa fase ci si collega al server FTP

-   `ftp.snets.it` per scaricare le chiavi pubbliche e private
    utilizzate dal server FOG e client FUSS per le automatizzazioni. Qui
    è assolutamente richiesto avere a portata di mano i parametri di
    accesso al server FTP quali `username` e `password`. Si hanno a
    disposizione `10` tentativi per inserire la `password` corretta.

-   `activateKeys.sh` copia la chiave pubblica appena scarivata sul
    server FUSS per permettere l'accesso passwordless al server FUSS da
    parte dell'installazione del server FOG e delle future installazioni
    dei client FUSS.

### install.sh

In questa fase si avvia la vera e propria installazione e configurazione
di FOG e del server FUSS. Le fasi sono:

-   Modifica del file di configurazione FOG

-   Aggiornamento dell'installazione FOG

-   Modifica della banca dati FOG

-   Import della banca dati FOG

-   Modifche al server FUSS

-   Personalizzazioni

`upgradeFog.sh` In questa fase sono aggiornati i parametri
`IP, Netmask, DNS` dell'installazione di FOG ed avviata la
reinstallazione di FOG, la modifica ed import finale del database di FOG
personalizzato. Questo passo può richiedere qualche minuto.

Al termine l'installazione continua dopo aver premuto un tasto sulla
tastiera, collegandosi via `ssh` al server FUSS ed avviando lo script
`editFussServer.sh`.

#### editFussServer.sh

In questa fase l'installazione si collega al server fuss ed applica
delle modifche alla configurazione del server FUSS. Le fasi sono:

-   Modifiche al DHCP

-   Modifiche al servizio TFTP

-   Modifiche al DNS

-   Import della banca dati FOG

-   Modifche al server FUSS

-   Personalizzazioni

`addDhcp.sh` aggiunge le modifiche al file `/etc/dhcp/dhcpd-added.conf`
per permettere una personalizzazione degli ambienti PXE da caricare in
base all'architettura del PC (x86 o x64, UEFI o Legacy). Al termined
esegue un commit di `etckeeper` per poter tenere traccia della modifica.
`adaptTftp.sh` modifica il file `/etc/dhcp/dhcpd.conf` commentando i
parametri `filename, next-server` e tutto il blocco `group` per
permettere ai PC client di ricevere gli ambienti PXE di FOG al boot.

`addDNS.sh` modifica il file `/var/cache/bind/db.local` aggiungendo
fogserver come `CNAME` e riavviando il servizio DNS `bind9`. Dopo 120 secondi i PC client potranno effettuare il boot da PXE FOG.

#### Personalizzazioni

L'ultima fase include alcune persionalizzazioni.
`changePxeBackground.sh` modifica il file png in
`/var/www/fog/service/ipxe/` utilizzato come sfondo dell'ambiente PXE.

![Ambiente PXE con sfondo personalizzato](not_registered.png)

`openLdapCustomization.sh` modifica il file
`/var/www/fog/lib/plugins/ldap/class/ldap.class.php` per permettere al
plugin openLDAP di funzionare correttamente come fa topic nel forum di
FOG<https://forums.fogproject.org/topic/13087/ldap-plugin-with-openldap/>

#### Accedere a FOG

Ora è possibile visitare tramite browser il link <http://localhost/fog/management> per accedere all'interfaccia web di FOG con la username e password come indicato al termine dello script `Username: fog` e `Password: password`.

![Accedere all'interfaccia di gestione di FOG](fog_management.png)

Il manuale ufficiale per la gestione di FOG si trova alla seguente pagina <https://wiki.fogproject.org/wiki/index.php?title=Managing_FOG>

Installazione guidata con screenshot
====================================

OPZIONALE: Impostare TAG e Queue vLAN
-------------------------------------

![Impostare TAG e/o Queue vLAN se previsto dal nostro ambiente
ProxMox](vlan_tag.png)

Tasto destro sul server FOG importato e selezionare `Start`
-----------------------------------------------------------

![Avviare con start](vm_imported.png)

Configurazione server con FOG\_setup.sh
---------------------------------------

![Dopo aver fatto il login con root:fuss avviare l'installazione con un
doppioclick su FOG\_setup.sh](fog_setup.png)

### Setup 1/3 Rete

![Inserito indirizzo IP 10.199.199.12, netmask 255.255.255.0 e indirizzo
server FUSS 10.199.199.11](setup.png)

### Setup 2/3 Hostname

![Inserito 251105vs02](setup_2-3.png)

### Setup 3/3 Nome scuola

![Inserito Scuola di test](setup_3-3.png)

### Premere un tasto per continuare

![Premere un tasto, sarà richiesta la password di root del server
FUSS](tasto_fuss_1.png)

### Password root FUSS

![Accettare il certificato ed inserire la password di root
FUSS](password_root_1.png)

### Connessione FTP al server: username

![Richiesta username per collegarsi al server ftp.snets.it e scaricare
le chiavi SSH](ftp_1.png)

### Connessione FTP al server: password

![Richiesta password per collegarsi al server ftp.snets.it e scaricare
le chiavi SSH](ftp_2.png)

### Attivazione chiavi SSH

![Premere un tasto per continuare](ssh_keys_1.png)

### Attivazione chiavi SSH

![Inserire la password root del server FUSS per attivare le chiavi SSH
scaricate](ssh_keys_2.png)

### Inizio configurazione FOG

![Modifica dei parametri FOG](fog_1.png)

### Aggiornamento FOG

![Installazione FOG](fog_2.png)

### Premere un tasto per continuare

![Premere un tasto, sarà richiesta la password di root del server
FUSS](tasto_fuss_2.png)

### Password root FUSS

![Inserire nuovamente la password di root FUSS](password_root_2.png)

### Modifiche al server FUSS

![L'installazione termina la configurazione FUSS](modifiche_fuss.png)

### Installazione terminata

![L'installazione è completata, tra 120 secondi i PC client potranno effettuare il boot da PXE FOG](termine.png)

Security Hardening
==================

Hardening server FOG
--------------------

Procedere con gli standard consigliati dal manuale tecnico di FUSS per
il hardening del server FOG. Si consiglia quantomento di modificare la
password di root da `fuss` ad una password più sicura.

Hardening FOG
-------------

La guida ufficiale per l'hardening di FOG si trova alla seguente pagina:
<https://wiki.fogproject.org/wiki/index.php?title=FOG_security>

### MySQL

È utile (e consigliato) prendere alcune misure per proteggere la banca
dati. MySQL viene fornito con un piccolo script che consente di
implementare un minimo di sicurezza di base per il database.
Semplicemente eseguire lo script, ma ASSICURATI di prendere nota delle
password che saranno impostate in quanto sarà necessario fornirle a FOG.
Eseguire lo script di installazione sicura di MySQL:
`sudo mysql_secure_installation` Leggere con attenzione cosa comunica lo
script, poiché è importante essere consapevoli delle proprie azioni! Lo
script permetterà di impostare una password di root per il database! Ora
impostare la nuova password ed assicurarsi di prenderne nota. FOG ne
avrà bisogno. Al termine eseguire `/etc/init.d/mysqld reload` Modificare
il file `/opt/fog/.fogsettings` ed aggiungere la password alla voce
`snmysqlpass=”`

### Proteggere le immagini

Quando FOG cattura un'immagine, crea uno o più file immagine per quel
computer. A seconda di come si utilizza il FOG, è possibile proteggere
la directory delle immagini. Poiché non è necessario per altri utenti
per accedere a questi file, limiteremo l'accesso a root e a FOG. Per
correggere l'esecuzione della cartella Images (supponendo che /images/
sia dove vengono salvate le immagini FOG) `chown -R fog:root /images/`
Poi fare per impostare i permessi `chmod -R 770 /images/` In teoria si
potrebbe (dovrebbe?) andare con un insieme più restrittivo di permessi,
tuttavia FOG potrebbe smettere di funzionare.

Configurazioni iniziali
=======================

Al termine dell'installazione di FOG nell'ambiente FUSS si consiglia di
configurare FOG.

Cluster FUSS/Gruppi FOG
-----------------------

I `cluster FUSS` in FOG sono semplicemente chiamati `Groups o Gruppi`.
Per facilitare l'automazione dell'installazione dei client si consiglia
di configurare i cluster/gruppi.

### Aggiungere Cluster FUSS/Gruppi in FOG

Aprire il browser ed accedere a FOG. Cliccare sul simbolo dei gruppi ed
aggiungere un gruppo con `Create New Group`.

### Lista gruppi vuota

![Cliccare in alto sul simbolo Groups, e sulla sinistra su
`Create New Group`](empty_group_list.png)

### Compilare i campi necessari

Inserire i dati nei campi e premere `Add` al termine. Si consiglia di
insrire i nomi dei `cluster FUSS` senza spazi tra le parole. e.g.
`AulaInformatica`, `AulaInsegnanti`, `Classi0` etc\...

![Inserito Group Name ed una descrizione, infine premere
Add](create_new_group.png)

Registrare i client in FOG
--------------------------

Dopo aver creato i `gruppi FOG`/`cluster FUSS` è necessario registrare i
client FUSS in FOG. In questa guida parleremo di due soluzioni delle
molteplici possibilità: `Registrazione manuale` (consigliata quando è
necessario impostare i parametri del BIOS manualmente per ogni singolo
PC) e `Registrazione tramite import`.

### Imposazioni BIOS pc client

Per poter utilizzare al meglio le automazioni di FOG in ambiente FUSS il
BIOS del client FUSS deve essere impostato per il
[`WakeOnLan`](https://it.wikipedia.org/wiki/Wake_on_LAN) ed il boot da
rete
[`PXE boot`](https://it.wikipedia.org/wiki/Preboot_Execution_Environment).

e.g. BIOS HP

![Avviamento ordine: Controller di rete in prima
posizione](hp_boot_network.png)

![Abilitare l'avvio da rete](hp_activate_network_boot.png)

![Sorgente in caso di wakeup remoto](hp_remote_wakeup_source.png)

![Sorgente in caso di wakeup remoto](hp_pxe_boot.png)

### Registrazione manuale

La registrazione manuale consiste nel registrare ogni singolo client dal
boot PXE di FOG. Al seguente link è possibile visionare il [video su
YouTube](https://youtu.be/PCzKY-gHIo8) che mostra la procedura (attivare
i sottotitoli in Italiano o Inglese per la descrizione dei singoli
passi).

Di seguito la registrazione manuale documentata con screenshot.

![Selezionare `Registrazione ed Inventario (completo)`](pxe_register.png)

![Inserire il `HOSTNAME` del PC in questione](pxe_hostname_inserted.png)

![È richiesto ora quale immagine associare a questo PC. Con `?` è
possibile mostrare la lista delle immagini disponibili. Inserendo il
numero corrispondente all'ID e.g. `1` si associa l'immagine
desiderata.](pxe_ask_image.png)

![È richiesto ora quale location associare a questo PC. Con `?` è
possibile mostrare la lista delle location disponibili. Inserendo il
numero corrispondente all'ID e.g. `1` si associa la location
desiderata.](pxe_ask_location.png)

![È richiesto ora quale `gruppo FOG/cluster FUSS` associare a questo PC.
Con `?` è possibile mostrare la lista dei `gruppo FOG/cluster FUSS`
disponibili. Inserendo il numero corrispondente all'ID e.g. `1` si
associa il `gruppo FOG/cluster FUSS` desiderato.](pxe_ask_group.png)

La registrazione chiederà dei parametri opzionali quali `snapins`,
`product key`, `il join al dominio`, `user primario`,
`tag aggiuntivo #1`, `tag aggiuntivo #2` e se eseguire il
deploy/installazione del PC. Premere `n` o semplicemente `INVIO` per
saltare questi parametri se non interessanti.

![Inserire i parametri opzioniali e alla fine decidere se iniziare con
l'installazione automatizzata del PC o meno, con `n` il deploy `NON`
sarà avviato](pxe_ask_deploy.png)

### Registrazione tramite import

La registrazione tramite import consiste nel scaricare la tabella dei
client da FOG e reimportare tale tabella con i dati dei nostri client.
Il file CSV che viene importato in FOG deve essere nel seguente formato,
e il file **non deve avere una riga di intestazione**. Segue la
descrizione delle singole colonne basilari, le conlonne segnate con
(OPZIONALE) possono essere vuote:

1.  Indirizzo MAC del PC client

2.  Nome HOST del PC client (deve essere inferiore a 15 caratteri,
    **non** deve includere i trattini bassi)

3.  (OPZIONALE) Descrizione

4.  (OPZIONALE) Product key Windows

5.  ID immagine FUSS (1 di FUSS 9 GOLD)

Maggiori informazioni nella [guida
ufficiale](https://wiki.fogproject.org/wiki/index.php/Managing_FOG#Method_4:_Importing_Host_Information).

Dopo l'import è necessario aggiungere i host ai
`gruppi FOG/cluster FUSS` e alla `Location`. Anche qui è possibile
procere con l'import.

Installazione dei client FUSS
-----------------------------

L'installazione dei client FUSS si avvia dall'interfaccia FOG per
singolo client oppure per `gruppo FOG/cluster FUSS`. Selezionare
dall'interfaccia la modalità desiderata ed avviare il deploy
dell'immagine associata al client con la freccia verde.

![Cliccare sulla freccia verde del singolo client per avviare il
deploy](fog_deploy_single_client.png)

![Cliccare sulla freccia verde de lgruppo per avviare il deploy per
tutto il `gruppo FOG/cluster FUSS`](fog_deploy_group.png)

![Cliccare sulla freccia verde del singolo client per avviare il
deploy](fog_deploy_add_task.png)

![Nella lista de gli `Active tasks`, sarà possibile seguire l'andamento
dell'installazione](fog_running_tasks.png)

Per la gestione è possibile seguire la guida ufficiale di FOG
<https://wiki.fogproject.org/wiki/index.php?title=Managing_FOG>.

Feedback e Supporto
===================

Per ulteriori domande o suggerimenti su miglioramenti degli script o
questa documentazione si prega di aprire un `Issue` su GitLab:
<https://gitlab.com/antonionardella/szme-ocsinventoryagent/issues>
