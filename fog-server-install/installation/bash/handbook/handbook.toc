\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {section}{\numberline {0.1}Introduzione}{2}{section.0.1}% 
\contentsline {section}{\numberline {0.2}Prerequisiti}{3}{section.0.2}% 
\contentsline {chapter}{\numberline {1}Checklist}{5}{chapter.1}% 
\contentsline {chapter}{\numberline {2}Panoramica di installazione}{7}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Import del server virtuale FOG in ProxMox}{7}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}Scaricare l'immagine del server virtuale FOG}{7}{subsection.2.1.1}% 
\contentsline {subsection}{\numberline {2.1.2}Importare l'immagine del server virtuale FOG in ProxMox}{7}{subsection.2.1.2}% 
\contentsline {subsection}{\numberline {2.1.3}OPZIONALE: Aggiungere tag VLAN}{8}{subsection.2.1.3}% 
\contentsline {subsection}{\numberline {2.1.4}Avviare il server virtuale FOG}{8}{subsection.2.1.4}% 
\contentsline {section}{\numberline {2.2}Configurazione server FOG}{9}{section.2.2}% 
\contentsline {section}{\numberline {2.3}Fasi dell'installazione}{9}{section.2.3}% 
\contentsline {subsection}{\numberline {2.3.1}FOG\_setup.sh}{9}{subsection.2.3.1}% 
\contentsline {subsection}{\numberline {2.3.2}setup.sh}{10}{subsection.2.3.2}% 
\contentsline {subsection}{\numberline {2.3.3}install.sh}{12}{subsection.2.3.3}% 
\contentsline {subsubsection}{editFussServer.sh}{13}{section*.2}% 
\contentsline {subsubsection}{Personalizzazioni}{14}{section*.3}% 
\contentsline {subsection}{\numberline {2.3.4}Accedere a FOG}{15}{subsection.2.3.4}% 
\contentsline {chapter}{\numberline {3}Installazione guidata con screenshot}{16}{chapter.3}% 
\contentsline {section}{\numberline {3.1}OPZIONALE: Impostare TAG e Queue vLAN}{17}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Tasto destro sul server FOG importato e selezionare \texttt {Start}}{18}{section.3.2}% 
\contentsline {section}{\numberline {3.3}Configurazione server con FOG\_setup.sh}{18}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}Setup 1/3 Rete}{19}{subsection.3.3.1}% 
\contentsline {subsection}{\numberline {3.3.2}Setup 2/3 Hostname}{20}{subsection.3.3.2}% 
\contentsline {subsection}{\numberline {3.3.3}Setup 3/3 Nome scuola}{21}{subsection.3.3.3}% 
\contentsline {subsection}{\numberline {3.3.4}Premere un tasto per continuare}{22}{subsection.3.3.4}% 
\contentsline {subsection}{\numberline {3.3.5}Password root FUSS}{23}{subsection.3.3.5}% 
\contentsline {subsection}{\numberline {3.3.6}Connessione FTP al server: username}{24}{subsection.3.3.6}% 
\contentsline {subsection}{\numberline {3.3.7}Connessione FTP al server: password}{25}{subsection.3.3.7}% 
\contentsline {subsection}{\numberline {3.3.8}Attivazione chiavi SSH}{26}{subsection.3.3.8}% 
\contentsline {subsection}{\numberline {3.3.9}Attivazione chiavi SSH}{27}{subsection.3.3.9}% 
\contentsline {subsection}{\numberline {3.3.10}Inizio configurazione FOG}{28}{subsection.3.3.10}% 
\contentsline {subsection}{\numberline {3.3.11}Aggiornamento FOG}{29}{subsection.3.3.11}% 
\contentsline {subsection}{\numberline {3.3.12}Modifiche al server FUSS}{30}{subsection.3.3.12}% 
\contentsline {subsection}{\numberline {3.3.13}Installazione terminata}{31}{subsection.3.3.13}% 
\contentsline {chapter}{\numberline {4}Security Hardening}{32}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Hardening server FOG}{32}{section.4.1}% 
\contentsline {section}{\numberline {4.2}Hardening FOG}{32}{section.4.2}% 
\contentsline {subsection}{\numberline {4.2.1}MySQL}{32}{subsection.4.2.1}% 
\contentsline {subsection}{\numberline {4.2.2}Proteggere le immagini}{33}{subsection.4.2.2}% 
\contentsline {chapter}{\numberline {5}Configurazioni iniziali}{34}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Cluster FUSS/Gruppi FOG}{34}{section.5.1}% 
\contentsline {subsection}{\numberline {5.1.1}Aggiungere Cluster FUSS/Gruppi in FOG}{34}{subsection.5.1.1}% 
\contentsline {subsection}{\numberline {5.1.2}Lista gruppi vuota}{35}{subsection.5.1.2}% 
\contentsline {subsection}{\numberline {5.1.3}Compilare i campi necessari}{35}{subsection.5.1.3}% 
\contentsline {section}{\numberline {5.2}Registrare i client in FOG}{36}{section.5.2}% 
\contentsline {subsection}{\numberline {5.2.1}Imposazioni BIOS pc client}{36}{subsection.5.2.1}% 
\contentsline {subsection}{\numberline {5.2.2}Registrazione manuale}{38}{subsection.5.2.2}% 
\contentsline {subsection}{\numberline {5.2.3}Registrazione tramite import}{44}{subsection.5.2.3}% 
\contentsline {section}{\numberline {5.3}Installazione dei client FUSS}{45}{section.5.3}% 
\contentsline {chapter}{\numberline {6}Tips and Tricks}{49}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Kernel per HP dc5800 e 7900p}{49}{section.6.1}% 
\contentsline {chapter}{\numberline {7}Feedback e Supporto}{50}{chapter.7}% 
