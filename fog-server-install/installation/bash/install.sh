#!/bin/bash
#### Filename: install.sh
#### Creation Date: 2019-03-29T11:58:22+01:00
####
#### Last modified by:   Antonio Nardella
#### Last modified time: 2019-03-29T11:59:02+01:00
####
#### Copyright: 2019 Antonio Nardella - progetti@antonionardella.it / Guggi
####
#### SPDX-License-Identifier: GPL-3.0-or-later

#### Description: This scripts is the main installation script
#### for a proxmox template FOG server in a FUSS environment
#### Part 1: Ask for school name (FOG Location)
#### Part 2: Edit .fogsettings and upgrade FOG
#### Part 3: Edit the default FOG MySQL dump
#### Part 4: Import the edited FOG MySQL dump to the database
#### Part 5: Login to FUSS server and set parameters (PXE, TFTP)
#### Part 6: FOG personalization
#### Part XerX: Reboot the FOG server
set -eo pipefail
INSTALL_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


# Part 1
cd $INSTALL_DIR
echo "Aggiornamento di FOG"
sleep $sleep_time
. ./scripts/upgradeFog.sh

# Part 2
cd $INSTALL_DIR
echo "Personalizzazione del database di FOG"
sleep $sleep_time
. ./scripts/editDmp.sh

# Part 3
cd $INSTALL_DIR
echo "Import del database FOG personalizzato"
sleep $sleep_time
. ./scripts/importDmp.sh

# Part 4
cd $INSTALL_DIR
echo "Modifiche al server FUSS"
sleep $sleep_time
. ./scripts/editFussServer.sh

# Part 5
cd $INSTALL_DIR
echo "Personalizzazione"
sleep $sleep_time
. ./personalization.sh

echo "##########################################################################"
echo "# Termine installazione                                                  #"
echo "#                                                                        #"
echo "# Hostname del server FOG configurato:       ${ANSWERS[hostname]}        #"
echo "# Indirizzo IP del server FOG configurato:   ${ANSWERS[address]}         #"
echo "# Nome della scuola configurato:             ${ANSWERS[schoolName]}      #"
echo "# Configurazione della banca dati FOG completata                         #"
echo "#          - Accesso a FOG: http://localhost/fog                         #"
echo "#          - Nome utente: fog                                            #"
echo "#          - Password:    password                                       #"
echo "##########################################################################"
echo ""
