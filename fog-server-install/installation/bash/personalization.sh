#!/bin/bash
#### Filename: personalization.sh
#### Creation Date: 2019-06-03T15:11:48+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-06-03T15:11:51+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later

# Description: This script runs different personalizations

# Eyecandy
## Change PXE boot background
cd $INSTALL_DIR
. ./scripts/fogPersonalization/changePxeBackground.sh

# FOG OpenLDAP support
# see https://forums.fogproject.org/topic/13087/ldap-plugin-with-openldap/
## Customize php class in line 541
cd $INSTALL_DIR
. ./scripts/fogPersonalization/openLdapCustomization.sh

# Update fog.fusssetup postInstallation script with updates
cd $INSTALL_DIR
. ./scripts/fogPersonalization/updatePostInstall.sh
