#### Filename: restartSSHd.sh
#### Creation Date: 2019-07-01T08:27:01+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-06-25T08:27:04+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later

set -eo pipefail

## restart SSHD
/usr/bin/ssh-keygen -A
systemctl restart sshd
