#!/bin/bash
#### Filename: upgradeFogtest.sh
#### Creation Date: 2019-03-29T17:18:47+01:00
####
#### Last modified by:   Antonio Nardella
#### Last modified time: 2019-03-29T17:18:50+01:00
####
#### Copyright: 2019 Antonio Nardella - progetti@antonionardella.it / Guggi
####
#### SPDX-License-Identifier: GPL-3.0-or-later
set -eo pipefail

# Define variables
FOG_SETTINGS_FILE=/opt/fog/.fogsettings

# edit /opt/fog/.fogsettings
# edit FOG server IP address
echo "Aggiornamento IP del server FOG nel file di configurazione di FOG"
sleep $sleep_time
sed -i.bak 's/10.199.199.12/'"${ANSWERS[address]}"'/g' $FOG_SETTINGS_FILE

# edit submask
echo "Aggiornamento Netmask del server FOG nel file di configurazione di FOG"
sleep $sleep_time
sed -i.bak 's/255.255.255.0/'"${ANSWERS[netmask]}"'/g' $FOG_SETTINGS_FILE

# edit dns server = FUSS server IP address
echo "Aggiornamento DNS del server FOG nel file di configurazione di FOG"
sleep $sleep_time
sed -i.bak 's/10.199.199.11/'"${ANSWERS[gateway]}"'/g' $FOG_SETTINGS_FILE

# update FOG
echo "Aggiornamento dell'installazione di FOG"
echo "Questa fase può richiedere alcuni minuti si prega di attendere il termine"
sleep $sleep_time
cd /root/Tools/fogproject-1.5.5/bin
bash installfog.sh -YX
