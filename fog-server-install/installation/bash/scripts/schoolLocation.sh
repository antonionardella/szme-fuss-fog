#!/bin/bash
#### Filename: schoolLocation.sh
#### Description: This scripts asks for the school name
#### and keeps it in the variable DLN
#### Copyright 2019 Antonio Nardella - progetti@antonionardella.it / Guggi
####
#### SPDX-License-Identifier: GPL-3.0-or-later
####
#### Version 0.0.1
set -eo pipefail

#	Define Variables
# Expand the task list with words that describe the task
declare TASK_LIST="schoolName"
declare -A ANSWERS


getinfo()
{
MSG_STR_SCHOOLNAME="Inserire il nome della scuola: (ad esempio Liceo G.Galilei)"
#
#
	# Work the list
	for task in $TASK_LIST
	do
		# Prepare the message string
		str=MSG_STR_${task^^}
		# Loop while input is empty
		while [ -z "${ANSWERS[$task]}" ]
		do
			echo ${!str}
			read -r input
			ANSWERS[$task]=$input
		done
	done
}

saveschoolname()
{
	echo ""
	echo "Il nome scuola è stato salvato"
	echo ""
}

clear
echo "##########################################################################"
echo "# Setup 3/3  Impostare nome scuola per il Location Plugin del server FOG #"
echo "##########################################################################"
echo ""
echo ""
echo ""

getinfo
echo ""
echo "Per piacere verificare il nome della scuola inserito!"
echo ""

while true; do
  read -p "Il nome inserito è corretto? [s/n]: " sn
  case $sn in
    [SsYy]* ) saveschoolname; return 0;;
    [Nn]* ) unset ANSWERS; declare -A ANSWERS; getinfo;;
        * ) echo "Per piacere premere s o n!";;
  esac
done
