#!/bin/bash

#### Filename: importDmptest.sh
#### Creation Date: 2019-03-29T13:43:55+01:00
####
#### Last modified by:   Antonio Nardella
#### Last modified time: 2019-03-29T13:43:58+01:00
####
#### Copyright: 2019 Antonio Nardella - progetti@antonionardella.it / Guggi
####
#### SPDX-License-Identifier: GPL-3.0-or-later


mysql -e "source /root/Tools/default.dmp"
