#### Filename: proxySetup.sh
#### Creation Date: 2019-06-03T12:46:28+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-06-03T12:46:33+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later
set -eo pipefail

export HTTP_PROXY=http://${ANSWERS[gateway]}:8080
export HTTPS__PROXY=$HTTP_PROXY
export FTP_PROXY=$HTTP_PROXY
export RSYNC_PROXY=$HTTP_PROXY
export NO_PROXY="localhost,127.0.0.1,localhost.localdomain,127.0.1.1,FOGIP"
