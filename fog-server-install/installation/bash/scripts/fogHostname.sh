#!/bin/bash
#### Filename: fogHostname.sh
#### Creation Date: 2019-06-05T08:58:03+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-06-05T08:58:06+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later
set -eo pipefail

#	Define Variables
# Expand the task list with words that describe the task
declare TASK_LIST="hostname"
declare -A ANSWERS

getinfo()
{
# Use the words from the task-list as string identifier
MSG_STR_HOSTNAME="Inserire il nome host (HOSTNAME) per il server FOG: (ad esempio 251101vs02)"
#
#	Action
#
# Work the list
for task in $TASK_LIST
do
  # Prepare the message string
  str=MSG_STR_${task^^}
  # Loop while input is empty
  while [ -z "${ANSWERS[$task]}" ]
  do
    echo ${!str}
    read input
    if [[ $input =~ ^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$ ]]; then
      ANSWERS[$task]=$input
   else
      #read input
      echo ""
      echo "Verificare formato inserito!!"
      echo ""
    fi
  done
done
echo
echo "Nome host inserito:"
for item in $TASK_LIST
do
  echo "$item: ${ANSWERS[$item]}"
done
}

setuphostname()
{
  # set hostname variable
  echo "Set hostname variable"
  NEWHOST=${ANSWERS[hostname]}


  # Change hostname routine
  # Assign existing hostname to §hostn
  echo "Assign existing hostname to §hostn"
  HOSTN=$(cat /etc/hostname)

  # Change
  echo "Change hostname in /etc/hosts & /etc/hostname"
  sed -i "s/$HOSTN/$NEWHOST/g" /etc/hosts
  sed -i "s/10.199.199.12/${ANSWERS[address]}/g" /etc/hosts
  sed -i "s/$HOSTN/$NEWHOST/g" /etc/hostname


  # Reload hostname
  ## Default
  invoke-rc.d hostname.sh start
  invoke-rc.d networking force-reload
  invoke-rc.d network-manager force-reload

  # systemD
  hostnamectl set-hostname $NEWHOST

  # Commit etckeeper this makes it possible to trace the edits in /etc
  if ! command -v etckeeper > /dev/null; then echo "etckeeper missing"; else etckeeper commit "edit hostname"; fi
}

clear
echo "##########################################################################"
echo "# Setup 2/3      Impostare il nome host (HOSTNAME) per il server FOG     #"
echo "##########################################################################"
echo ""

getinfo
echo "Per piacere verificare il hostname inserito!"

while true; do
  read -p "Queste informazioni sono corrette? [s/n]: " sn
  case $sn in
		[SsYy]* ) setuphostname; return 0;;
    [Nn]* ) unset ANSWERS; declare -A ANSWERS; getinfo;;
        * ) echo "Per piacere premere s o n!";;
  esac
done
