#!/bin/bash
#### Filename: ethInterface.sh
#### Creation Date: 2019-04-05T17:29:18+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-04-05T17:34:28+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it / Guggi
####
#### SPDX-License-Identifier: GPL-3.0-or-later

#### Version 0.0.69
set -eo pipefail

#	Define Variables
# Expand the task list with words that describe the task
declare TASK_LIST="address netmask gateway"
declare -A ANSWERS

getinfo()
{
MSG_STR_ADDRESS="Inserire l'indirizzo IP per il server FOG: (ad esempio 10.0.123.12) e premere INVIO"
MSG_STR_NETMASK="Inserire la netmask per la rete: (ad esempio 255.255.255.0) e premere INVIO"
MSG_STR_GATEWAY="Inserire l'indirizzo IP del server FUSS: (ad esempio 10.0.123.11) e premere INVIO"
#
#	Action
#
	# Work the list
	for task in $TASK_LIST
	do
		# Prepare the message string
		str=MSG_STR_${task^^}
		# Loop while input is empty
		while [ -z "${ANSWERS[$task]}" ]
		do
			echo ${!str}
			read input
      if [[ $input =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        ANSWERS[$task]=$input
     else
        #read input
        echo ""
        echo "Verificare formato inserito!!"
        echo ""
      fi
    done
	done
	echo
	echo "Configurazione inserita:"
	for item in $TASK_LIST
	do
    echo "$item: ${ANSWERS[$item]}"
	done
}

setupnetworkmanager()
{
	ETH=$(ip link | awk -F: '$0 !~ "lo|vir|wl|^[^0-9]"{print $2;getline}')
	nmcli con mod "Wired connection 1" ipv4.addresses ${ANSWERS[address]}/24
	nmcli con mod "Wired connection 1" ipv4.gateway ${ANSWERS[gateway]}
	nmcli con mod "Wired connection 1" ipv4.dns ${ANSWERS[gateway]}
	nmcli con mod "Wired connection 1" ipv4.method manual
	nmcli con mod "Wired connection 1" connection.autoconnect yes
	nmcli con up "Wired connection 1"
	echo ""
	echo "La configurazione di NetworkManager è stata salvata."
	echo ""
}

clear
echo "##########################################################################"
echo "# Setup 1/3      Impostare configurazione IP statica per il server FOG   #"
echo "##########################################################################"
echo ""

getinfo
echo "Per piacere verificare la configurazione inserita!"

while true; do
  read -p "Queste informazioni sono corrette? [s/n]: " sn
  case $sn in
		[SsYy]* ) setupnetworkmanager; systemctl restart network-manager; return 0;;
    [Nn]* ) unset ANSWERS; declare -A ANSWERS; getinfo;;
        * ) echo "Per piacere premere s o n!";;
  esac
done
