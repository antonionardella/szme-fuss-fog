#!/bin/bash

#### Filename: addSSH.sh
#### Creation Date: 2019-06-20T11:31:03+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-06-20T11:31:06+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later
set -eo pipefail

# Ask if human is present
declare -A FTPUSER

getinfo()
{
echo ""
echo "Inserire il nome utente per collegarsi al server FTP: ftp.snets.it"
read -r input
if [[ $input =~ ^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$ ]]; then
  FTPUSER=$input
else
  #read input
  echo ""
  echo "Verificare formato inserito!!"
  echo ""
fi
}

connectftp()
{
	echo ""
	echo "Il nome utente inserito sarà utilizzato per collegarsi al server FTP. Sarà richiesto di inserire la password manualmente"
	echo ""

  # Get ssh keys from FTP
  mkdir -p /root/.ssh/
  # wget -O /root/.ssh/id_portachiavi --user="$FTPUSER" --ask-password ftp://ftp.snets.it/technik/KONZEPTE/FUSS/FOG/PROXMOX_TEMPLATE/auths/ssh_keys/id_portachiavi
  # wget -O /root/.ssh/id_portachiavi.pub --user="$FTPUSER" --ask-password ftp://ftp.snets.it/technik/KONZEPTE/FUSS/FOG/PROXMOX_TEMPLATE/auths/ssh_keys/id_portachiavi.pub
  # wget --retry-connrefused --waitretry=30 -P /root/.ssh/ --user="$FTPUSER" --ask-password ftp://93.39.126.227/technik/KONZEPTE/FUSS/FOG/PROXMOX_TEMPLATE/auths/ssh_keys/*
  n=0
  until [ $n -ge 10 ]
  do
     wget --retry-connrefused --waitretry=30 -P /root/.ssh/ --user="$FTPUSER" --ask-password ftp://93.39.126.227/technik/KONZEPTE/FUSS/FOG/PROXMOX_TEMPLATE/auths/ssh_keys/* && break  # substitute your command here
     n=$[$n+1]
     sleep $sleep_time
  done


  ## Set ssh keys permission
  chmod 700 /root/.ssh
  chmod 644 /root/.ssh/id_portachiavi.pub
  chmod 600 /root/.ssh/id_portachiavi
}

clear
echo "##########################################################################"
echo "# Scaricare chiavi SSH per automatizzare il join del client FUSS         #"
echo "##########################################################################"
echo ""
echo ""
echo ""

getinfo
echo ""
echo "Per piacere verificare il nome utente inserito!"
echo ""

while true; do
  read -p "Il nome inserito è corretto? [s/n]: " sn
  case $sn in
    [SsYy]* ) connectftp; return 0;;
    [Nn]* ) unset FTPUSER; declare -A FTPUSER; getinfo;;
        * ) echo "Per piacere premere s o n!";;
  esac
done
