#!/bin/bash
#### Filename: getKernel.sh
#### Creation Date: 2019-07-19T14:15:37+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-07-19T14:15:40+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later
echo "FIX KERNELS"
FILE=/var/www/html/fog/service/ipxe/init_154.xz
if test -f "$FILE"; then
    echo "Kernels already present!"
  else
    cd /tmp
    wget --no-check-certificate https://fogproject.org/binaries1.5.4.zip
    unzip binaries1.5.4.zip
    cp packages/inits/init.xz /var/www/html/fog/service/ipxe/init_154.xz
    cp packages/kernels/bzImage /var/www/html/fog/service/ipxe/bzImage_154
    rm -rf /tmp/packages
fi
