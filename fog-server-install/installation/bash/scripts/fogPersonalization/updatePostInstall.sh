#!/bin/bash
#### Filename: updatePostInstall.sh
#### Creation Date: 2019-07-16T14:14:32+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-07-16T14:14:32+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later

# Overwrite FOG postInstall script in /images/postdownloadscripts/fog.fusssetup
cat /usr/share/szme-fuss-fog/fog-server/post-image-scripts/fog.fusssetup > /images/postdownloadscripts/fog.fusssetup
