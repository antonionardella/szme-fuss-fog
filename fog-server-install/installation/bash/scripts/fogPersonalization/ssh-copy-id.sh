#!/bin/bash
#### Filename: getKernel.sh
#### Creation Date: 2019-07-19T14:15:37+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-07-19T14:15:40+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later

ssh-copy-id -i /root/.ssh/id_portachiavi root@proxy 