#!/bin/bash
#### Filename: changePxeBackground.sh
#### Creation Date: 2019-06-03T15:02:04+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-06-03T15:02:08+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later

# Define variables
BCK_FOLDER=/usr/share/szme-fuss-fog/images/pxeboot/
BCK_FILENAME=bg.png

cp $BCK_FOLDER$BCK_FILENAME /var/www/fog/service/ipxe/bg.png
