#!/bin/bash
#### Filename: openLdapCustomization.sh
#### Creation Date: 2019-06-05T14:30:32+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-06-05T14:30:35+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later
echo "FIX LDAP Plugin"
if find /var/www/fog/lib/plugins/ldap/class/ldap.class.php -not -newer 2018-12-01; then
    echo "Apply Patch"
    sed -i.bak '541 s/^/\/\//' /var/www/fog/lib/plugins/ldap/class/ldap.class.php
    sed -i.bak '541a $userDN = $user;' /var/www/fog/lib/plugins/ldap/class/ldap.class.php
    # Install php-ldap
    apt-get -y update
    DEBIAN_FRONTEND=noninteractive DEBIAN_PRIORITY=critical apt-get -q -y -o "Dpkg::Options::=--force-confdef" -o "Dpkg::Options::=--force-confold" install php-ldap
    systemctl restart apache2
else
  echo "LDAP Already Patched"
fi
