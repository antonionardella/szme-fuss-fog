#### Filename: activateKeys.sh
#### Creation Date: 2019-06-25T08:27:01+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-06-25T08:27:04+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later

set -eo pipefail

# Ask if human is present
read -n 1 -s -r -p "Premere un tasto sulla tastiera per continuare, sarà richiesta la password di root del server FUSS"

## ssh-copy-id to activate id_portachiavi on FUSS server
ssh-copy-id -i /root/.ssh/id_portachiavi.pub root@${ANSWERS[gateway]}
