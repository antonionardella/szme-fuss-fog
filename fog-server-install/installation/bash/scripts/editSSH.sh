#### Filename: editSSH.sh
#### Creation Date: 2019-06-20T14:13:35+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-06-20T14:13:38+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later
set -eo pipefail

# Autorized keys for ssh
sed -i '/AuthorizedKeysFile/s/^#*//g' /etc/ssh/sshd_config

# Reload SSH
echo "Restart SSH"
systemctl restart sshd
