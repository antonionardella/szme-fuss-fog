#!/bin/bash
#### Filename: addDNS.sh
#### Creation Date: 2019-04-05T13:34:08+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-04-05T13:34:18+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it / Guggi
####
#### SPDX-License-Identifier: GPL-3.0-or-later
#### Description: Adds FOG server as DNS CNAME

# Define variables
DB_LOCAL_FILE="/var/cache/bind/db.local"

# Add fogserver as CNAME after proxy occurrence
echo "Aggiunge fogserver come CNAME dopo la riga che contiene proxy"
sleep 3
sed -i "/^proxy/a \$TTL 604800     ; 1 week\nfogserver               CNAME   $FOGHOSTNAME" $DB_LOCAL_FILE
sed -i "/^proxy/a \$TTL 3600       ; 1 hour\n$FOGHOSTNAME             A       $FOGIP" $DB_LOCAL_FILE

# Edit bind9 serial
echo "Modifica il seriale di bind9"
sleep 3
sed -i "s/\s*[0-9]\+\s*;\s*Serial/                       $FOGDATE         ; Serial/" $DB_LOCAL_FILE

# Reload bind9
echo "Reload bind9"
sleep 3
systemctl reload bind9
systemctl restart bind9
