#!/bin/bash
#### Filename: addGitlab.sh
#### Creation Date: 2019-06-03T11:05:52+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-06-03T11:06:52+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later
set -euo pipefail

# Define squid configuration file we want to edit
# From the documentation the file "/etc/squid3/squid-added-repo.conf" is not edited by fuss-server
SQUID_FILE="/etc/squid3/squid-added-repo.conf"

# Add gitlab.com ACL for squid to make it possible to update these scripts
echo "Aggiungere ACL a squid"
sleep 3
echo "acl repositories url_regex gitlab.com" >> $SQUID_FILE
echo "acl repositories url_regex *.snets.it" >> $SQUID_FILE

# Reload squid
echo "Reload di squid3"
sleep 9
systemctl reload squid3
systemctl restart squid3

# Commit etckeeper this makes it possible to trace the edits in /etc
if ! command -v etckeeper > /dev/null; then echo "etckeeper missing"; else etckeeper commit "addGitlab.sh"; fi
