#!/bin/bash
#### Filename: fogAllowedHost.sh
#### Creation Date: 2019-06-03T11:28:18+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-06-03T11:28:22+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later
set -euo pipefail

# Define variables
FIREWALL_FILE="/etc/fuss-server/firewall-allowed-lan-hosts"

# Add FOG Server to firewall allowed hosts
echo "Aggiungiere il server FOG ai host abilitati sul firewall"
sleep 3
echo FOGIP:FOGHOSTNAME >> $FIREWALL_FILE

# Reload firewall
echo "Reload del firewall"
sleep 3
systemctl reload firewall

# Commit etckeeper this makes it possible to trace the edits in /etc
if ! command -v etckeeper > /dev/null; then echo "etckeeper missing"; else etckeeper commit "fogAllowedHost.sh"; fi
