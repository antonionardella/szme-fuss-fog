#!/bin/bash
#### Filename: install.sh
#### Creation Date: 2019-03-29T11:58:22+01:00
####
#### Last modified by:   Antonio Nardella
#### Last modified time: 2019-03-29T11:59:02+01:00
####
#### Copyright: 2019 Antonio Nardella - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later

#### Description: This scripts is the main installation script
#### for a proxmox template FOG server in a FUSS environment

declare TASK_LIST="address netmask gateway"
declare -A ANSWERS

getinfo()
{
MSG_STR_ADDRESS="Inserire l'indirizzo IP per il server FOG: (ad esempio 10.0.123.12)"
MSG_STR_NETMASK="Inserire la netmask per la rete: (ad esempio 255.255.255.0)"
MSG_STR_GATEWAY="Inserire l'indirizzo IP del server FUSS: (ad esempio 10.0.123.11)"
#
#	Action
#
	# Work the list
	for task in $TASK_LIST
	do
		# Prepare the message string
		str=MSG_STR_${task^^}
		# Loop while input is empty
		while [ -z "${ANSWERS[$task]}" ]
		do
			echo ${!str}
			read input
      if [[ $input =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        ANSWERS[$task]=$input
     else
        #read input
        echo ""
        echo "Verificare formato inserito!!"
        echo ""
      fi
    done
	done
	echo
	echo "Configurazione inserita:"
	for item in $TASK_LIST
	do
    echo "$item: ${ANSWERS[$item]}"
	done
}
startplaybook()
{
  echo "\"fogServerIP\":\"${ANSWERS[address]}\",\"fussFogSubnet\":\"${ANSWERS[netmask]}\",\"fussServerIP\":\"${ANSWERS[gateway]}\"}" >>  setup.json
  echo -e "[fussserver]" >> /etc/ansible/hosts
	echo -e "${ANSWERS[gateway]}" >> /etc/ansible/hosts
	#ansible-playbook -s fogsetup.yml --extra-vars "@setup.json"
  ansible-playbook -s ./scripts/fogsetup.yml -c local --check  --extra-vars "@setup.json" # debug
	#ansible-playbook -s fussetup.yml -c fussserver --extra-vars "@setup.json"
	ansible-playbook -s ../fussserverPlaybook/fusssetup.yml -c fussserver --check  --extra-vars "@setup.json" # debug
  exit 0
}

echo "Impostare configurazione IP statica per il server FOG"
echo ""

getinfo
echo "Per piacere verificare la configurazione inserita!"

while true; do
  read -p "Queste informazioni sono corrette? [s/n]: " sn
  case $sn in
    [SsYy]* ) startplaybook; return 0;;
    [Nn]* ) unset ANSWERS; declare -A ANSWERS; getinfo;;
        * ) echo "Per piacere premere s o n!";;
  esac
done
