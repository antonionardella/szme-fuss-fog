#!/bin/sh

#### Description: Prepare a FUSS Client image for FOG
#### First restore the original FUSS Client image from iso2.fuss.bz.it with clonezilla
#### Then prepare and personalize with this script
#### Copyright 2019 Antonio Nardella - progetti@antonionardella.it / Guggi
####
#### SPDX-License-Identifier: GPL-2.0
####
#### Version 0.0.7

exec 2>&1
exec > >(tee -a /var/log/fussgoldimage.log)
# Everything below will go to the file '/var/log/fussgoldimage.log'

# Setup

## Purge FUSS client
## This is a good practice to avoid installation errors
## during the automated setup, if the source image is not clean
## e.g. fuss-9-amd64-20181121-img.tar: fuss-client -p -r has not been executed successfully
echo Purge FUSS client
fuss-client -p -r
## e.g. fuss-9-amd64-20181121-img.tar has a lock file, blocking all apt-get usage
rm /var/lib/dpkg/lock

## Debian 9 full upgrade
echo debian 9 full upgrade
apt-get -y update
DEBIAN_FRONTEND=noninteractive DEBIAN_PRIORITY=critical sudo apt-get -q -y -o "Dpkg::Options::=--force-confdef" -o "Dpkg::Options::=--force-confold" full-upgrade

## Download and install FOG Client from FOG server on the subnet
## HUMAN INTERACTION NEEDED: The installation has to be finished manually at the end
echo apt purge mono-complete
apt-get -y purge mono-complete
rm /etc/apt/sources.list.d/mono-xamarin.list
apt autoremove -y
echo apt install mono-complete
apt-get -y install mono-complete
echo download FOG Client
wget -O /usr/share/szme-fuss-fog/fog-client/SmartInstaller.exe http://fogserver/fog/management/index.php?node=client

## Copy SSH keys from FUSS server
##
## !!! THIS PART CAN BE ADAPTED TO COPY SSH KEYS FROM USB STICK: PORTACHIAVI
##
rm /root/.ssh
mkdir /root/.ssh
scp fogserver:/root/.ssh/id_portachiavi /root/.ssh/
scp fogserver:/root/.ssh/id_portachiavi.pub /root/.ssh/

## Set ssh keys permission
chmod 700 /root/.ssh
chmod 644 /root/.ssh/*.pub
chmod 600 /root/.ssh/id*

# Personalize

## Install packages
echo "Install preferred packages"
apt-get -y install jq mc sshpass apt-transport-https unp dmidecode libxml-simple-perl libcompress-zlib-perl libnet-ip-perl libwww-perl libdigest-md5-perl libnet-ssleay-perl libcrypt-ssleay-perl libnet-snmp-perl libproc-pid-file-perl libproc-daemon-perl net-tools libsys-syslog-perl pciutils smartmontools read-edid nmap

## Install OCSInventory agent
echo "Install OCSInventory agent"
ARCHITECTURE=$(dpkg --print-architecture)
RELEASE=$(lsb_release -c | cut -f2)
DEBIAN_FRONTEND=noninteractive gdebi --non-interactive /usr/share/szme-fuss-fog/packages/ocs/$RELEASE/ocsinventory-agent_2.4.2-3_bpo9+1_$ARCHITECTURE.deb

## Set wallpaper
cp /usr/share/szme-fuss-fog/images/wallpaper/fuss-9-fog-wallpaper.svg /usr/share/fuss-artwork/wallpapers/
xfconf-query -c xfce4-desktop \
  -p /backdrop/screen0/monitor0/workspace0/last-image \
  -s /usr/share/fuss-artwork/wallpapers/fuss-9-fog-wallpaper.svg

## Set root partition label to root (for FOG post-install script)
ROOTPARTITION=$(df / | grep /dev | awk -F" " '{print $1}')
e2label $ROOTPARTITION root

## Grub images
cp /usr/share/szme-fuss-fog/images/grub/bg_fuss_fog_16x9.png /usr/share/desktop-base/active-theme/grub/grub-16x9.png
cp /usr/share/szme-fuss-fog/images/grub/bg_fuss_fog_4x3.png /usr/share/desktop-base/active-theme/grub/grub-4x3.png
update-grub

beep -f 500 -l 700
beep -f 480 -l 400
beep -f 470 -l 250
echo "##############################"
echo "##          WARNING         ##"
echo "##############################"
echo ""
echo "TO USE OCS Inventory WITH THIS IMAGE"
echo "REMEMBER TO COPY THE server.crt TO:"
echo ""
echo "/usr/share/szme-fuss-fog/ocs"
echo ""
echo "DIRECTORY"
echo ""
echo "##############################"
echo "##          WARNING         ##"
echo "##############################"
beep -f 500 -l 700
beep -f 480 -l 400
beep -f 470 -l 250

sleep 10
# Cleanup

## Remove DHCP leases
rm /var/lib/dhcp/dhclient*
rm /etc/network/interfaces.d/setup
rm /etc/machine-id

## Clean bash history
history -c
rm /root/.bash_history
touch /root/.bash_history

## Clean apt
apt clean
apt autoclean
